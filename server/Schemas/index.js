const graphql = require("graphql");
const {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLInt,
  GraphQLString,
  GraphQLList,
} = graphql;
const rickAndMortyData = require("../MOCK_DATA.json");

const RickAndMortyType = require("./TypeDefs/RickAndMortyType");

const RootQuery = new GraphQLObjectType({
  name: "RootQueryType",
  fields: {
    getAll: {
      type: new GraphQLList(RickAndMortyType),
      args: { id: { type: GraphQLInt } },
      resolve(parent, args) {
        return rickAndMortyData;
      },
    },
  },
});

module.exports = new GraphQLSchema({ query: RootQuery });
