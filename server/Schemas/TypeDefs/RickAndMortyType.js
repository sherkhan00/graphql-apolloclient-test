const graphql = require("graphql");
const { GraphQLObjectType, GraphQLInt, GraphQLString } = graphql;

const RickAndMortyType = new GraphQLObjectType({
  name: "RickAndMorty",
  fields: () => ({
    id: { type: GraphQLInt },
    name: { type: GraphQLString },
    status: { type: GraphQLString },
    species: { type: GraphQLString },
    lastKnownLocation: { type: GraphQLString },
    firstSeenLocation: { type: GraphQLString },
    image: { type: GraphQLString },
  }),
});

module.exports = RickAndMortyType;
