import React, { useEffect, useState } from "react";
import { useQuery, gql } from "@apollo/client";
import { LOAD_RICK_AND_MORTY } from "../GraphQL/Queries";
import Card from "./Card";

function GetRickAndMorty() {
  const { error, loading, data } = useQuery(LOAD_RICK_AND_MORTY);
  const [rickAndMortyList, setRickAndMortyList] = useState([]);

  useEffect(() => {
    if (data) {
      setRickAndMortyList(data.getAll);
    }
  }, [data]);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error {error.message}</p>;

  return (
    <div className="container d-flex flex-wrap justify-content-center">
      {rickAndMortyList.map((val) => (
        <Card key={val.id} {...val} />
      ))}
    </div>
  );
}

export default GetRickAndMorty;
