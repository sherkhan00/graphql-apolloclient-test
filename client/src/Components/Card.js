const Card = ({
  name,
  status,
  image,
  species,
  lastKnownLocation,
  firstSeenLocation,
}) => {
  return (
    <div className="card w-25 m-3">
      <img src={image} className="card-img-top" alt="character" />
      <div className="card-body">
        <h5 className="card-title, mb-0">{name}</h5>
        <p className="card-text">
          {status} - {species}
        </p>

        <h6 className="card-subtitle text-muted">Last known location:</h6>
        <p className="card-text">{lastKnownLocation}</p>
        <h6 className="card-subtitle text-muted">First seen in: </h6>
        <p className="card-text">{firstSeenLocation}</p>
      </div>
    </div>
  );
};

export default Card;
