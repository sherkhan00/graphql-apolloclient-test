import { gql } from "@apollo/client";

export const LOAD_RICK_AND_MORTY = gql`
  query {
    getAll {
      id
      name
      status
      species
      lastKnownLocation
      firstSeenLocation
      image
    }
  }
`;
